#!/usr/bin/env python

from datetime import datetime
from datetime import timedelta
import sys

workouts = {
    'A': ['Squat', 'Bench Press', 'Barbell Row'],
    'B': ['Squat', 'Overhead Press', 'Dead Lifts']
}

workout = {
    'type': 'A',
    'date': datetime.today(),
    'status': 'completed'
}

def getWorkoutCalendar(starting_date, number_of_weeks):
    workout_dates = []
    workout_dates.append(starting_date)
    previous_date = starting_date
    while(len(workout_dates) < number_of_weeks*3):
        if (previous_date.isoweekday() == 5):
            time_delta = timedelta(days=3)
        else:
            time_delta = timedelta(days=2)
        next_date = previous_date + time_delta
        workout_dates.append( next_date )
        previous_date = next_date
        
    return workout_dates

def formatDate(workout_date):
    return datetime.strftime(workout_date, "%d-%m-%Y");

def displayWorkoutCalendar(workout_dates):
    week_number = 1
    weight = 10
    print("{:*^42}".format("StrongLifts 5x5 Plan"))
    print('*'*42)
    print("Workout A: {}-{}-{}".format(*workouts['A']))
    print("Workout B: {}-{}-{}".format(*workouts['B']))
    for i in range(len(workout_dates) // 3):
        week_days = [ formatDate(workout_date) for workout_date in workout_dates[ i*3:(i+1)*3 ] ]
        week_str = "Week: {}".format(week_number)
        weight_str = "Weight: {} kg".format(weight)
        workout_type = 'A' if (week_number % 2 == 1) else 'B'
        workout_str = "Workout: {}".format(workout_type)
        print("{:<14}{:^14}{:>14}".format(week_str, weight_str, workout_str))
        print('-'*42)
        print("{:-^14}{:-^14}{:-^14}".format(*week_days))
        print('-'*42)
        status_boxes = "{:-^14}".format(' '*4)*3
        print(status_boxes)
        print('-'*42)
        week_number += 1
        weight += 5

    

if __name__ == '__main__':
    number_of_weeks = int(sys.argv[1])
    today = datetime.today()
    workout_calendar = getWorkoutCalendar(today, number_of_weeks)
    displayWorkoutCalendar(workout_calendar)